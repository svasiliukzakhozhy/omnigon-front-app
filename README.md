# Social Feed React App

## Description 

Widget renders N last social posts from JSON feed. The widget pull updates from the feed with the given interval and update the display list by removing old items and displaying the new ones.

## Running the application

### Clone repository

```sh
$ git clone https://bitbucket.org/svasiliukzakhozhy/omnigon-front-app.git
$ cd omnigon-front-app
$ npm install
```

### Start the development

```sh
$ npm start
```
