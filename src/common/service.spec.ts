import axios from 'axios';

import { getPosts, loadPosts } from '../common/service';
import { resPostsFromBackend, posts } from '../mock-data';

jest.mock('axios');


describe('Services', () => {

  it('should load posts', () => {
    (axios.get as any).mockResolvedValue({ data: resPostsFromBackend });
    loadPosts(2, 'twitter').then(({ data }) => {
      expect(data).toEqual(resPostsFromBackend)
    });
  });

  it('should Posts', async () => {
    (axios.get as any).mockResolvedValue({ data: resPostsFromBackend });
    const res = await getPosts(2, 'twitter');
    expect(res).toEqual(posts);
  });

  it('Get posts with error fetching data', async () => {
    (axios.get as any).mockResolvedValue(new Error());
    const res = await getPosts(2, 'twitter');
    expect(res).toEqual([]);
  });

  it('Get posts with null date', async () => {
    const resPostsFromBackendNullable = resPostsFromBackend;
    resPostsFromBackendNullable[0].created_at = '';
    (axios.get as any).mockResolvedValue({ data: resPostsFromBackendNullable });
    const res = await getPosts(2, 'twitter');
    expect(res[0].createdAt).toEqual(null);
  });

});