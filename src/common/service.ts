import axios from 'axios';

import { IPost, IPostFromBackend } from './interfaces';


export async function loadPosts(limit: number, network: string): Promise<{ data: IPostFromBackend[] }> {
  return await axios.get('http://api.massrelevance.com/MassRelDemo/kindle.json', {
    params: {
      limit,
      network,
    }
  });
}

export async function getPosts(limit: number, network = 'twitter'): Promise<IPost[]> {
  try {
    const res = await loadPosts(limit, network);
    return res.data.map(backPost => ({
      id: backPost.id,
      createdAt: backPost.created_at ? new Date(backPost.created_at) : null,
      userName: backPost.user.name,
      message: backPost.text,
    }));
  } catch (e) {
    return [];
  }
}
