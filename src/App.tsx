import React, { Component } from 'react';

import SocialPosts from './social-posts/SocialPosts';
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="App">
        <SocialPosts limit={2} network="twitter" interval={10000} />
      </div >
    );
  }
}

export default App;
