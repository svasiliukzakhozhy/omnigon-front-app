import React from 'react';
import { shallow } from 'enzyme';

import { Post } from '../social-posts/Post';
import { posts } from '../mock-data';


describe('Post component', () => {

  it('renders all props', () => {
    const post = posts[0];
    const wrapper = shallow(<Post {...post} />);
    expect(wrapper.contains(<div className="post-card-user-name">{post.userName}</div>)).toBeTruthy();
    expect(wrapper.contains(<div className="post-card-date">{post.createdAt && new Date(post.createdAt).toLocaleString()}</div>)).toBeTruthy();
    expect(wrapper.contains(<div className="post-card-content">{post.message}</div>)).toBeTruthy();
  });

  it('shouldn\t render card-date', () => {
    const post = posts[0];
    post.createdAt = null;
    const wrapper = shallow(<Post {...post} />);
    expect(wrapper.contains(<div className="post-card-date">{post.createdAt && new Date(post.createdAt).toLocaleString()}</div>)).toBeFalsy();
  });

});