import React, { Component } from 'react';

import '../styles/social-posts.scss';
import { Post } from './Post';
import { IPost } from '../common/interfaces';
import { getPosts } from '../common/service';


interface ISocialPostsProps {
  limit: number;
  network?: 'twitter' | 'facebook' | 'instagram' | 'google_plus' | 'rss';
  interval: number;
}

interface ISocialPostsState {
  posts: IPost[];
}

export default class SocialPosts extends Component<ISocialPostsProps, ISocialPostsState> {
  state = { posts: [] };

  private intervalId!: NodeJS.Timeout;

  private async updateSocialPosts(): Promise<void> {
    const posts = await getPosts(this.props.limit, this.props.network);
    this.setState({ posts });
  }

  public componentDidMount() {
    this.updateSocialPosts();

    this.intervalId = setInterval(() => {
      this.updateSocialPosts();
    }, this.props.interval || 10000);
  }

  public componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  private get posts(): Array<JSX.Element> | JSX.Element {
    if (!this.state.posts.length) {
      return (<div className="empty-data"></div>);
    }

    return this.state.posts.map((post: IPost) => <Post key={post.id} {...post} />);
  }

  public render() {
    return (
      <div className="social-posts">
        {this.posts}
      </div>
    );
  }
}