import React, { FunctionComponent, memo } from "react";

import { IPost } from "../common/interfaces";


export const Post: FunctionComponent<IPost> = ((props) => {
  let cardDate: JSX.Element | null = null;

  if (props.createdAt) {
    cardDate = (
      <div className="post-card-date">{props.createdAt && props.createdAt.toLocaleString()}</div>
    );
  }

  return (
    <div className="post-card">
      <div className="post-card-header">
        <div className="post-card-user-name">{props.userName}</div>
        {cardDate}
      </div>
      <div className="post-card-content">{props.message}</div>
    </div>
  )
});