import React from 'react';
import { shallow, mount } from 'enzyme';

import { Post } from '../social-posts/Post';
import SocialPosts from './SocialPosts';
import { posts } from '../mock-data';

jest.useFakeTimers();

const socialPostsProps = {
  limit: 3,
  interval: 1000,
}

describe('SocialPosts component', () => {

  it('Shouldn\'t be a any post when mounting a component', () => {
    const wrapper = shallow(<SocialPosts {...socialPostsProps} />);
    expect(wrapper.find(Post).length).toEqual(0);
  });

  it('should render posts after set state', () => {
    const wrapper = shallow(<SocialPosts {...socialPostsProps} />);
    wrapper.setState({ posts });
    expect(wrapper.find(Post).length).toEqual(posts.length);
  });

  it('should call clearInterval()', () => {
    const wrapper = mount(<SocialPosts {...socialPostsProps} />);
    wrapper.unmount();
    expect(clearInterval).toHaveBeenCalledWith(expect.any(Number));
  });

});