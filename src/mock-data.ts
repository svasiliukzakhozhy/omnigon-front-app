import { IPost, IPostFromBackend } from "./common/interfaces";


export const posts: IPost[] = [
  {
    createdAt: new Date('Fri Dec 29 19:15:04 +0000 2017'),
    id: 946821889648980000,
    message: 'The battle is life or death',
    userName: "Lori King",
  },
  {
    createdAt: new Date('Fri Dec 19 19:15:04 +0000 2017'),
    id: 946821889648980020,
    message: 'The battle ',
    userName: "Lori King1",
  },
];

export const resPostsFromBackend: IPostFromBackend[] = [
  {
    created_at: 'Fri Dec 29 19:15:04 +0000 2017',
    id: 946821889648980000,
    text: 'The battle is life or death',
    user: { name: "Lori King", }
  },
  {
    created_at: 'Fri Dec 19 19:15:04 +0000 2017',
    id: 946821889648980020,
    text: 'The battle ',
    user: { name: "Lori King1", }
  },
];